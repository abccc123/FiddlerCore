﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cross_siteScripting
{
    class Program
    {
        static void Main(string[] args)
        {
            TAntiXss.testc();
            Console.ReadLine();
        }
        static bool CheckKeyWord(string _sWord)
        {
            if (_sWord.Contains("/"))
            {
                string[] sp = _sWord.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                if (sp.Length >= 2)
                {
                    _sWord = sp[1];
                }
            }

            bool result = true;
            string StrKeyWord = "default.aspx|preview.aspx"; //System.Configuration.ConfigurationSettings.AppSettings["validateFilterUrl"].ToString();
            if (string.IsNullOrEmpty(StrKeyWord))
            {
                return result;
            }
            string[] patten1 = StrKeyWord.Split('|'); //模式1 : 对应Sql注入的可能关键字

            foreach (string sqlKey in patten1)
            {
                if (_sWord.IndexOf(sqlKey, StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    //只要存在一个可能出现Sql注入的参数,则直接退出
                    result = false;
                    break;
                }
            }
            return result;
        }
    }
}
