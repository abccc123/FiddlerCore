﻿using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cross_siteScripting
{
    //   http://blog.csdn.net/yanzhibo/article/details/41524493 编码函数

//应该使用的场景

//示例/模式

//HtmlEncode

//不可信的输入被用作html输出，被分配给一个html属性除外

//<a href="http://www.contoso.com">Click Here [Untrusted input]</a>

//HtmlAttributeEncode

//不可信的输入作为一个html属性

//<hr noshade size=[Untrusted input]>

//JavaScriptEncode

//不可信的输入作为一个JavaScript上下文

//<script type="text/javascript">

//…

//[Untrusted input]

//…

//</script>

//UrlEncode

//不可信的输入作为一个url(例如作为一个查询参数的值)

//<a href="http://search.msn.com/results.aspx?

//q=[Untrusted-input]">Click Here!</a>

//VisualBasicScriptEncode

//不可信的输入作为一个visual basic上下文

//<script type="text/vbscript" language="vbscript">

//…

//[Untrusted input]

//…

//</script>

//XmlEncode

//不可信的输入作为一个xml输出，除了把它作为一个xml节点的属性

//<xml_tag>[Untrusted input]</xml_tag>

//XmlAttributeEncode

//不可信的输入作为一个xml的属性

//<xml_tag attribute=[Untrusted input]>Some Text</xml_tag>
   public class TAntiXss
    {

        public static void testc(){
            var html = "<a href=\"#\" onclick=\"alert();\">aaaaaaaaa</a>javascript<P><IMG SRC=javascript:alert('XSS')><javascript>alert('a')</javascript><IMG src=\"abc.jpg\"><IMG><P>Test</P>";

            string safeHtml = AntiXss.HtmlAttributeEncode(html);
            Console.WriteLine(safeHtml);
            Console.WriteLine(AntiXss.HtmlEncode("<input type=\"aaa\"/>"));
            Console.WriteLine(AntiXss.JavaScriptEncode("function aa(){return 1;}"));
            Console.WriteLine(AntiXss.UrlEncode("http://stackoverflow.com/questions/27422605/capture-raw-https-request-and-response-with-fiddlercore"));


            Console.WriteLine(AntiXss.XmlEncode("function aa(){return 1;}"));
           // Console.WriteLine(AntiXss.JavaScriptEncode("function aa(){return 1;}"));
        }
    }
}
